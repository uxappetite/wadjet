#!/usr/bin/env python3
import requests

clientID     = "36107516bb774529bb18452c466e47d7"
clientSecret = "c0e00ff5c32e4742a0f63d7f75021284"
accessToken  = "6126996.3610751.2848636e60784b00aa96870ec2804bbc"
userID		 = "6126996"

# Aditional stuff
# https://api.instagram.com/v1/users/self/?access_token=ACCESS-TOKEN
avatarURL    = "https:\/\/igcdn-photos-h-a.akamaihd.net\/hphotos-ak-xaf1\/t51.2885-19\/11427348_1645886208987839_922218733_a.jpg"
username     = "gavricci"
full_name    = "Alex Gavricci"

#############
# USING API #
#############

##### AUTHORIZATION #####

# Step 1 - redirect user
# https://api.instagram.com/oauth/authorize/?client_id=CLIENT_ID&redirect_uri=http://uxappetite.com/apps&response_type=code&scope=relationships

# Step 2 - read a code from response
# code=486008e6a7134a5bab74c99839e2ab3f

# Step 3 - retrieve an access_token
#    curl -F 'client_id=36107516bb774529bb18452c466e47d7' \
#     -F 'client_secret=c0e00ff5c32e4742a0f63d7f75021284' \
#     -F 'grant_type=authorization_code' \
#     -F 'redirect_uri=http://uxappetite.com/apps' \
#     -F 'code=486008e6a7134a5bab74c99839e2ab3f' \
#     https://api.instagram.com/oauth/access_token

# RESPONSE:
# {
# 	"access_token":"6126996.3610751.2848636e60784b00aa96870ec2804bbc",
# 	"user":
# 	{
# 		"username":"gavricci",
# 		"bio":"Bla-bla-bla...",
# 		"website":"",
# 		"profile_picture":"https:\/\/igcdn-photos-c-a.akamaihd.net\/....jpg",
# 		"full_name":"Alex Gavricci",
# 		"id":"6126996"
# 	}
# }


###### FEED ######

#TODO: read it from storage
# min_post_id = "0"
min_post_id = "1052449421301636013_1489879085"

# Get list of posts LATER than min_id
r = requests.get("https://api.instagram.com/v1/users/self/feed?access_token=" + accessToken + "&min_id=" + min_post_id)
response = r.json()

# Check if we have no error
if response['meta']['code'] == 200:

	# TODO: save it to storage
	min_post_id = response['data'][0]['id']

	print('Instagram Feed:')

	for post in response['data']:
		print()
		print('Post ID:', post['id'])
		print('Author:', post['user']['username'])
		print('Posted (UNIX time):', post['caption']['created_time'])
		print('Image URL:', post['images']['standard_resolution']['url'], post['images']['standard_resolution']['width'], 'x', post['images']['standard_resolution']['height'])

		likeCount = post['likes']['count']
		print(likeCount, 'likes', '(you too)' if post['user_has_liked'] else '')

		if likeCount > 0 and likeCount < 11:
			likeString = ''

			for (i, like) in enumerate(post['likes']['data']):
				likeString += ', ' if i > 0 and i < likeCount - 1 else ' and ' if i > 0 and i == likeCount - 1 else ''
				likeString += like['username']

			print(likeString, 'like this')

		print('Caption:', post['caption']['text'])
		print()
else:
	print('ERROR: server has responded with', response['meta']['code'], 'code')