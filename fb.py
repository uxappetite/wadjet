"""
Script working with facebook api
Created by Dmitry Kutsenko.
"""
import facebook
# import requests
from config import Config


def some_action(post):
    """ Here you might want to do something with each post. E.g. grab the
    post's message (post['message']) or the post's picture (post['picture']).
    In this implementation we just print the post's created time.
    """
    print(post)
    print('----')
    print(post['caption'])
    print('----')
    print(post['description'])
    print('----')


def run():
    # You'll need an access token here to do anything.  You can get a temporary one
    # here: https://developers.facebook.com/tools/explorer/
    fb = Config().facebook
    # 'CAACEdEose0cBAIAlSwILcddFFbDXtKUWHw6qmq6YcmEfVOdcwSrDOQ0arrZB8n0E4MGazTWEEvLjd82uWQEV3dZAhxQUTT74Jfa2fa7tMzL5xK7s9Hg6tZB49Jzgj05KyNt187ZCvmuymsiIxnhsdFY799ZCcq0H8IQDwo1Y01C6dmwoRvN642ZBG874GWyI7lbzZC29RrhZCgZDZD'
    # Look at Bill Gates's profile for this example by using his Facebook id.
    user = 'me'

    # facebook.get_app_access_token(fb['app_id'], fb['app_secret']))
    # api.access_token = api.get_object('oauth/access_token', client_id=fb['app_id'], client_secret=fb['app_secret'], grant_type=fb['credentials'])

    import requests

    response = requests.request("GET",
                                "https://graph.facebook.com/oauth/access_token",
                                params = {
                                    'client_id': fb['app_id'],
                                    'client_secret': fb['app_secret'],
                                    'grant_type': fb['credentials'],
                                    'redirect_uri': 'http://uxappetite.com',
                                })

    token = fb['token']
    # response.text[13:]
    api = facebook.GraphAPI(token)

    print(response.text)
    print(token)

    profile = api.get_object(user)
    posts = api.get_connections(profile['id'], 'home')

    print(posts)

# Wrap this block in a while loop so we can keep paginating requests until
# finished.
# while True:
#    # try:
#        # Perform some action on each post in the collection we receive from
#        # Facebook.
#        # [some_action(post=post) for post in posts['data']]
#        # Attempt to make a request to the next page of data, if it exists.
#        # posts = requests.get(posts['paging']['next']).json()
#    # except KeyError:
#        # When there are no more pages (['paging']['next']), break from the
#        # loop and end the script.
#        # break

if __name__ == '__main__':
    print(run())
